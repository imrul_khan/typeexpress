import * as express from 'express';
import * as bodyParser from "body-parser";
import * as path from "path";
import {WebRoute} from './routes/web';
import {ApiRoute} from './routes/api';

require("dotenv").config({ path: path.join(__dirname + "/../", ".env") });

const app = express();

//View Engine Setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/',WebRoute);
app.use('/api',ApiRoute);

app.set("port", process.env.PORT || 3000);
app.set("env", process.env.NODE_ENV || "development");


export default app;