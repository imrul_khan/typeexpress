import * as express from 'express';
import IndexController from '../controllers/IndexController';

const Route = express.Router();

//Declearing Controller instances
let indexController = new IndexController();

//all web routes
Route.get('/',indexController.index);




export {Route as WebRoute}