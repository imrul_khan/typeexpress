import * as express from 'express';

const Route = express.Router();

Route.get('/', function(req, res, next){
    res.status(200)
       .json({status:"ok"});
});

export {Route as ApiRoute}