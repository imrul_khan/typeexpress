import * as sqlite from 'sqlite3';
const sqlite3 = sqlite.verbose();

import * as path from 'path';
require('dotenv').config({path: path.join(__dirname+'/../../', '.env')});

let dbFile:string = path.join(process.env.DB_HOST,process.env.DB_NAME);

console.log(dbFile);

let db: sqlite.Database = new sqlite3.Database(dbFile, (err:any)=>{
    if(err){
        return console.error('Error in connecting SQLite DB: '+err.message);
    }else{
        console.log('Connected to SQLite DB');
    }
});

module.exports = db;