"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const web_1 = require("./routes/web");
const api_1 = require("./routes/api");
require("dotenv").config({ path: path.join(__dirname + "/../", ".env") });
const app = express();
//View Engine Setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', web_1.WebRoute);
app.use('/api', api_1.ApiRoute);
app.set("port", process.env.PORT || 3000);
app.set("env", process.env.NODE_ENV || "development");
exports.default = app;
//# sourceMappingURL=app.js.map