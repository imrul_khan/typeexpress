"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const IndexController_1 = require("../controllers/IndexController");
const Route = express.Router();
exports.WebRoute = Route;
//Declearing Controller instances
let indexController = new IndexController_1.default();
//all web routes
Route.get('/', indexController.index);
//# sourceMappingURL=web.js.map