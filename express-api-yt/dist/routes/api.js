"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const Route = express.Router();
exports.ApiRoute = Route;
Route.get('/', function (req, res, next) {
    res.status(200)
        .json({ status: "ok" });
});
//# sourceMappingURL=api.js.map