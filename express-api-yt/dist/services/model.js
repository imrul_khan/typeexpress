const DB = require('./dbconnect');
DB.serialize(() => {
    let SQL = `CREATE TABLE Persons (
                                        PersonID int,
                                        LastName varchar(255),
                                        FirstName varchar(255),
                                        Address varchar(255),
                                        City varchar(255) 
                                    );`;
    DB.run(SQL);
});
DB.close((err) => {
    if (err) {
        console.log(err.message);
    }
    else {
        console.log('DB connection Closed');
    }
});
//# sourceMappingURL=model.js.map