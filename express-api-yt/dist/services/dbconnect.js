"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sqlite = require("sqlite3");
const sqlite3 = sqlite.verbose();
const path = require("path");
require('dotenv').config({ path: path.join(__dirname + '/../../', '.env') });
let dbFile = path.join(process.env.DB_HOST, process.env.DB_NAME);
console.log(dbFile);
let db = new sqlite3.Database(dbFile, (err) => {
    if (err) {
        return console.error('Error in connecting SQLite DB: ' + err.message);
    }
    else {
        console.log('Connected to SQLite DB');
    }
});
module.exports = db;
//# sourceMappingURL=dbconnect.js.map